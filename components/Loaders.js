import { css } from "@emotion/core";
import ClipLoader from "react-spinners/ClipLoader";

export default function Loaders(){
    const override = css`
    display: block;
    margin: 0 auto;
    border-color: #00008b;
  `;


  class AwesomeComponent extends React.Component {
  constructor(props) {
    super(props);
    props.state = {
      loading: true
    };
  }

}

  return (
      <div className="sweet-loading">
        <ClipLoader
          css={override}
          size={150}
          color={"#123abc"}
        />
      </div>
    );

}