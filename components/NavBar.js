import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';


export default function NavBar(){

	return(
        <React.Fragment>
		<Navbar className="color-nav navbar navbar-expand-lg" variant="dark" expand="lg" id="navHome">
			<Link href="/">
                    <img src="/savingSemilImg.jpg"
                    onMouseOver={e => (e.currentTarget.src = "/savingSemilAni.gif")}
                    onMouseOut={e => (e.currentTarget.src = "/savingSemilImg.jpg")}
                    id="navLogo" className="img-fluid updateHover" />
			</Link>
			<Navbar.Toggle aria-controls="responsive-navbar-nav" />
			<Navbar.Collapse id="responsive-navbar-nav">
				<Nav className="ml-auto">
					<Link href="#updates">
						<a className="nav-link navSelects" role="button">Updates</a>
					</Link>
                    <Link href="#gallery">
                        <a className="nav-link navSelects" role="button">Gallery</a>
                    </Link>
                    <Link href="#forDonations">
						<a className="nav-link navSelects" role="button">For Donations</a>
					</Link>
					<Link href="/thank-you">
						<a className="nav-link navSelects" role="button" id="thanks"><strong>Thank You!</strong></a>
					</Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
        </React.Fragment>
	)
}