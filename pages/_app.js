import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from '../components/NavBar';

function MyApp({ Component, pageProps }) {
  return(
  <React.Fragment>
	  <NavBar/>
	  <Component {...pageProps} />
  </React.Fragment>
)
  
}

export default MyApp
