import { Container } from 'react-bootstrap';
import Head from 'next/head';

export default function FirstUpdate(){
	return(
		<React.Fragment>
			<Head>
		      <title>Saving Semil | First Update</title>
		    </Head>
			<Container fluid className="text-center">
				<h1 className="updateIntro"><strong>December 8, 2020</strong></h1>
				<br/>
				<br/>
				<p className="updateContent"> 
					"Semil just finished his 1st and 2nd sessions of chemotherapy and
					he's currently at home getting himself healthy and positive. &#128512;
				</p>
				<p className="updateContent">
					We also just started the campaign for Semil's cash donations today, December 7, 2020. &#128525;&#128525;
				</p>
				<p className="updateContent"> 
					Please <a href="https://saving-semil-ph.vercel.app/#forDonations" className="links"><strong>click this link</strong></a> to see how you can help
					Semil through donations. &#128521;
				</p>
				<br/>
				<br/>

				<img src="/chemo1.jpg" id="chemo1" />
				<p>1st chemotherapy session last November 11, 2020.</p>
				<img src="/chemo2.jpg" id="chemo2" />
				<p>2nd chemotherapy session last December 2, 2020.</p>
				<br/>
				<br/>
			</Container>
		</React.Fragment>
	)
}