import { Container, Card, CardDeck, Col, Carousel } from 'react-bootstrap';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import Loaders from '../components/Loaders';
import Link from 'next/link';
import SweetAlert from '../components/SweetAlert';

export default function Home(){
  return(
    <React.Fragment>
    <Head>
      <title>Saving Semil</title>
    </Head>
    <Container fluid>
    <h5 className="intro" id="cashDonations">
    Total donations as of 12/16/2020:
    <br/>
    &#8369;50,845.00
    </h5>
    </Container>
    <Container fluid className="text-center">
    <Container fluid>
        <Container fluid className="content">
          <h1 className="bolds">Welcome to our page!</h1>
          <p className="content">
            We all know, our "Kuya Semil" as a good friend and companion to all of us.
            He is like our "handyman", ready to help, especially with our fellow engineers.
            But now, it is time that we will be the "handyman" in his life, for him to get back on his feet,
            for his family, and for him to live life to the fullest.
            <br/>
            <br/>
            Aside from moral support and prayers, we are helping Semil to raise funds for his medications.
            Semil's family is in great need of financial assistance to support his medications,
            especially with his CHEMOTHERAPY sessions.
            <br/>
            <br/>
            Any amount, big or small, could make a difference for him.
            <br/>
            Let's all help to save his life and bring back our "Kuya Semil".
          </p>
        </Container>
        <img src="/semilCartoon.png" className="img-fluid rounded mx-auto d-block" id="semilCartoon"/>
    </Container>

    <Container fluid>
    <h1 className="bolds">Semil and his condition.</h1>
      <p className="content">
      Semil started having symptoms as early as July 2018, he was on continuous consultation
      with different doctors to try and find out the real problem.
      It was just recently that he was diagnosed by colon cancer after a major operation in his colon.
      In October 2020, Semil was officially diagnosed with Stage 3 Colon cancer and
      was advised by his doctor to do chemotherapy immediately.
      <br/>
      <br/>
      Semil is an out-patient and doing his medications in Providence Hospital in Quezon City.
      He is currently undergoing a series of chemotherapy that requires 12 sessions to complete.
      The chemotherapy consists of both IV infusion and oral is a hefty
      30,000 php per session plus all his expenses for transportation.
      <br/>
      <br/>
      Given the unfortunate circumstances,
      Semil is still hopeful and positive that he will overcome this challenge in his life.
      Our donations and prayers can help him and his family greatly in this battle against cancer.
      </p>
    </Container>

    <Container fluid id="updates" className="cardContent">
    <h1 className="bolds">Updates</h1>
    <CardDeck className="cardContent">
    <Link href="/firstupdate">
      <Card className="updateHover" border="dark">
        <Card.Img variant="top" className="cardImage" src="/firstUpdate.jpg" />
        <Card.Body>
          <Card.Title>December 8, 2020 | Update</Card.Title>
          <Card.Text>
            Click for more details.
          </Card.Text>
        </Card.Body>
      </Card>
    </Link>
      <Card border="dark">
        <Card.Img variant="top" src="https://picsum.photos/id/0/1800/890" />
        <Card.Body>
          <Card.Title>Saving Semil</Card.Title>
          <Card.Text>
            Further updates soon to come. &#128516; 
          </Card.Text>
        </Card.Body>
      </Card>
      <Card border="dark">
        <Card.Img variant="top" src="https://picsum.photos/id/0/1800/890" />
        <Card.Body>
          <Card.Title>Saving Semil</Card.Title>
          <Card.Text>
            Further updates soon to come. &#128516;
          </Card.Text>
        </Card.Body>
      </Card>
      </CardDeck>
    </Container>

      <Container fluid className="cardContent" id="gallery">
       <h1 className="bolds">Gallery</h1>
        <Carousel className="content" id="carousel">
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/sem.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil1.jpg"
              alt="TSemil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil2.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil4.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil5.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil6.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil7.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil9.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil10.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil11.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil12.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil13.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil14.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil15.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil16.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil17.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil18.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil19.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil20.jpg"
              alt="Semil"
            />
          </Carousel.Item>

          <Carousel.Item>
            <img
              className="d-block w-100"
              src="/semil21.jpg"
              alt="Semil"
            />
          </Carousel.Item>
        </Carousel>
      </Container>

      <Container fluid id="forDonations" className="cardContent">
        <h1 className="bolds">For Donations</h1>
        <br/>
        <br/>
        <Container fluid>
        <h3>You can send your donations on the following accounts:</h3>
        <br/>
          <img src="/bdo.png" className="accounts"/>
          <h4>
          Christine Joy Ciabal
          <br/>
          Acct. No.: 001 930 330 373 
          </h4>
          <br/>
          <br/>
          <img src="/bpi.png" className="accounts"/>
          <h4>
          Julian Christopher Macalindol
          <br/>
          Acct. No.: 372 954 3938
          </h4>
          <br/>
          <br/>
          <img src="/paymaya.png" className="accounts2"/>
          <h4>
          Julian Christopher Macalindol
          <br/>
          Acct. No.: 0998 575 9259
          </h4>
          <br/>
          <br/>
          <img src="/gcash.png" className="accounts2"/>
          <h4>
          Christine Joy Ciabal
          <br/>
          Acct. No.: 0995 477 2562
          </h4>
          <br/>
          <br/>
        </Container>
      </Container>
      <div className="footer-copyright text-center py-3">
        <Container fluid>
          &copy; 2020 | Webpage hosted by:
          <br/>
          <a href="https://vercel.com/" target="_blank"><img src="https://logovtor.com/wp-content/uploads/2020/10/vercel-inc-logo-vector.png" id="vercel" /></a>
        </Container>
      </div>
      </Container>
    </React.Fragment>
    )
}
