import { Card, CardColumns, Container } from 'react-bootstrap';

export default function ThankYou(){
	return(
		<Container fluid className="text-center content">
			<h1><strong>THANK YOU FOR YOUR DONATIONS!</strong>&#128512;</h1>
			<br/>
			<CardColumns>
				<Card.Img variant="top" className="cardImage" src="/thank/1.png" />
				<Card.Img variant="top" className="cardImage" src="/thank/2.jpg" />
				<Card.Img variant="top" className="cardImage" src="/thank/3.jpg" />
				<Card.Img variant="top" className="cardImage" src="/thank/4.jpg" />
				<Card.Img variant="top" className="cardImage" src="/thank/5.jpg" />
			</CardColumns>
		</Container>
	)
}